const express = require('express');
const app = express();
const knex = require('knex');
const bodyParser = require('body-parser')
const cors = require('cors');
const multer=require('multer');
const bcrypt = require('bcrypt-nodejs');

const storage = multer.diskStorage({
    destination:function (req,file,cb) {
        cb(null,'./uploads/');
    },
    filename:function (req,file,cb) {
        cb(null,new Date().toISOString().substring(0,10)+file.originalname);
    }
});

const fileFilter= (req,file,cb)=>{
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
        cb(null,true)
    }else {
        cb(new Error('unsupported file'),false)
    }
}
const upload = multer({storage:storage,fileFilter:fileFilter});

const db = knex({
    client: 'pg',
    connection: {
        host: '127.0.0.1',
        user: 'postgres',
        password: 'admin',
        database: 'lawfirmdb'
    }
});

app.use(bodyParser.json());
app.use(cors());
app.use('/uploads',express.static('uploads'))
app.use(bodyParser.urlencoded({extended: true}));

app.get('/',(req,res)=>{
    res.json('Hello...')
})
app.get('/appointments', (req, res)=> {
    db.select('*').from('appointments')
        .then(data=> {
            res.json(data);
        })
        .catch(err=> {
            res.status(400).json("Unable to fetch");
        })
})

app.post('/books', (req, res)=> {
    const {fname, lname, date, mobile_no, emailid, message} =req.body;
    db('appointments')
        .returning('*')
        .insert({
            fname: fname,
            lname: lname,
            ap_date: date,
            contact_no: mobile_no,
            email_id: emailid,
            message: message
        })
        .then(data=> {
            res.json(data)
        })
        .catch(error=> {
            res.status(400).json('error')
        })
})

app.post('/update/appointment', (req, res)=> {
    const {ap_id, c_date, ap_slot} = req.body;
        db('appointments')
            .where('appoint_id', '=', ap_id)
            .update({
                time_slot: ap_slot,
                ap_date: c_date,
                confirm_appoint: 'confirmed'
            })
            .then(data => {
                res.json(data)
            })
            .catch(error=> {
                res.status(400).json('Error in updating date')
            })
})

app.get('/appointments/today',(req,res)=>{
    db('appointments')
        .where('ap_date','=',new Date().toISOString().substring(0,10))
        .select('*')
        .then(data=>{
            res.json(data)
        })
        .catch(error =>{
            res.status(400).json('no record found')
        })
})

app.get('/appointments/confirm',(req,res)=>{
    db('appointments')
        .where('confirm_appoint','=','confirmed')
        .select('*')
        .then(data=>{
            res.json(data)
        })
        .catch(error =>{
            res.status(400).json('no record found')
        })
})

app.get('/appointments/waiting',(req,res)=>{
    db('appointments')
        .where('confirm_appoint',null)
        .select('*')
        .then(data=>{
            res.json(data)
        })
        .catch(error =>{
            res.status(400).json('no record found')
        })
})

app.post('/client/add',(req,res)=>{
    const {fname,lname,gender,mobile,email_id,occupation,entry_date,dob}=req.body;
    db('clients')
        .whereNot('email_id',email_id)
        .orWhereNot('mobile',mobile)
        .then(
            db('clients')
                .returning('*')
                .insert({
                    fname:fname,
                    lname:lname,
                    gender:gender,
                    mobile:mobile,
                    email_id:email_id,
                    occupation:occupation,
                    entry_date:entry_date,
                    dob:dob
                })
                .then(data=>{
                    res.json(data)
                })
                .catch(error =>{
                    res.status(400).json('existed')
                })
        )
        .catch(error=>{
            res.status(400).json('error in insert')
        })

})

app.get('/clients',(req,res)=>{
    db('clients')
        .select('c_id','fname','lname')
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('Unable to get clients')
        })
})

app.post('/clients/view',(req,res)=>{
    db('clients')
        .where('c_id','=',req.body.c_id)
        .select('*')
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('Unable to get client record')
        })

})

app.post('/client/update',(req,res)=>{
    const {id,fname,lname,dob,gender,mob,email,occupation}=req.body;
    db('clients')
        .whereNot('email_id',email)
        .orWhereNot('mobile',mob)
        .then(
            db('clients')
                .where('c_id','=',id)
                .update({
                    fname:fname,
                    lname:lname,
                    dob:dob,
                    gender:gender,
                    mobile:mob,
                    email_id:email,
                    occupation:occupation
                })
                .then(data=>{
                    res.json(data)
                })
                .catch(error=>{
                    res.status(400).json('existed')
                })
        )
        .catch(error=>{
            res.status(400).json('not update')
        })


})

app.post('/case/add',(req,res)=>{
    const {case_title,s_date,e_date,result,client_name,email_id,mobile,occupation,entry_date}=req.body;
    db('cases')
        .returning('*')
        .insert({
            case_title:case_title,
            s_date:s_date,
            e_date:e_date,
            result:result,
            client_name:client_name,
            email_id:email_id,
            mobile:mobile,
            occupation:occupation,
            entry_date:entry_date
        })
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('case not inserted')
        })
})

app.get('/cases/',(req,res)=>{
    db('cases')
        .select('cs_id','case_title','result')
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('data not found')
        })
})

app.get('/cases/counts/',(req,res)=>{
    db('cases')
        .where('result','=','won')
        .count('*')
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('No counts')
        })
})
app.get('/awards/counts/',(req,res)=>{
    db('awards')
        .count('*')
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('No counts')
        })
})
app.get('/clients/counts/',(req,res)=>{
    db('clients')
        .count('*')
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('No counts')
        })
})

app.post('/cases/view',(req,res)=>{
    db('cases')
        .where('cs_id','=',req.body.cs_id)
        .select('*')
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('Unable to get case record')
        })

})

app.post('/case/update',(req,res)=>{
    const {cs_id,case_title,s_date,e_date,result,client_name,email_id,mobile,occupation}=req.body;
    db('cases')
        .where('cs_id','=',cs_id)
        .update({
            case_title:case_title,
            s_date:s_date,
            e_date:e_date,
            result:result,
            client_name:client_name,
            email_id:email_id,
            mobile:mobile,
            occupation:occupation
        })
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('Not Updated')
        })
})

app.post('/award/add',(req,res)=>{
    const {a_name,e_name,e_date,receive,describe,entry_date}=req.body;
    db('awards')
        .returning('*')
        .insert({
          a_name:a_name,
            a_date:e_date,
            event_name:e_name,
            receive_form:receive,
            a_describe:describe,
            entry_date:entry_date
        })
        .then(data=>{
            res.json(data)
        })
        .catch(err=>{
            res.status(400).json('Not inserted')
        })
})

app.get('/awards/',(req,res)=>{
    db('awards')
        .select('a_id','a_name')
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('no records found')
        })
})

app.post('/awards/view',(req,res)=>{
    db('awards')
        .where('a_id','=',req.body.a_id)
        .select('*')
        .then(data=>{
            res.json(data)
        })
        .catch(err=>{
            res.status(400).json('no records found')
        })
})

app.post('/awards/update',(req,res)=>{
    const {a_id,a_name,e_name,e_date,receive,describe,entry_date}=req.body;
    db('awards')
        .where('a_id','=',a_id)
        .update({
            a_name:a_name,
            a_date:e_date,
            event_name:e_name,
            receive_form:receive,
            a_describe:describe,
            entry_date:entry_date
        })
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('not updated')
        })
})

app.post('/practice/add',(req,res)=>{
    const {title,describe}=req.body;
    db('practice')
        .whereNot('title',title)
        .then(db('practice')
            .returning('*')
            .insert({
                title:title,
                description:describe
            })
            .then(data=>{
                res.json(data)
            })
            .catch(error=>{
                res.status(400).json('existed')
            })
        )
        .catch(error=>{
            res.status(400).json('Error in inserted')
        })
})

app.get('/practice/viewall',(req,res)=>{
    db('practice')
        .select('*')
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('no record found')
        })
})

app.post('/practice/view',(req,res)=>{
    db('practice')
        .where('id','=',req.body.id)
        .select('*')
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('not found')
        })
})

app.post('/practice/update',(req,res)=>{
    const {id,title,describe}=req.body;
    db('practice')
        .whereNot('title',title)
        .then(
            db('practice')
                .where('id','=',id)
                .update({
                    title:title,
                    description:describe
                })
                .then(data=>{
                    res.json('updated')
                })
                .catch(error=>{
                    res.status(400).json('existed')
                })
        )
        .catch(error=>{
            res.status(400).json('not updated')
        })
})

app.post('/practice/delete',(req,res)=>{
    db('practice')
        .where('id','=',req.body.id)
        .delete('*')
        .then(data=>{
            res.end()
        })
        .catch(res.status(400).json('not deleted'))
})

app.post('/attorney/add',upload.single('attorneyImage'),(req,res)=>{
    db('attorney')
        .returning('*')
        .insert({
            a_name:req.body.name,
            a_post:req.body.designation,
            a_details:req.body.details,
            a_image:req.file.path
        })
        .then(res.send('attorney registered'))
        .catch(error=>{
            res.status(400).send('not registered')
        })
})

app.get('/attorney/all',(req,res)=>{
   db('attorney')
       .select('*')
       .then(data=>{
           res.json(data)
       })
       .catch(error=>{
           res.status(400).json('not found')
       })
})

app.post('/attorney/view',(req,res)=>{
    db('attorney')
        .where('id','=',req.body.id)
        .select('*')
        .then(data=>{
            res.json(data)
        })
        .catch(console.log)
})

app.post('/attorney/update',(req,res)=>{
        db('attorney')
            .where('id','=',req.body.id)
            .update({
                a_name:req.body.name,
                a_post:req.body.designation,
                a_details:req.body.details,
            })
            .then(res.send('attorney updated'))
            .catch(error=>{
                res.status(400).send('not update')
            })
})

app.post('/attorney/update/image',upload.single('attorneyImage'),(req,res)=>{
    db('attorney')
        .where('id','=',req.body.id)
        .update({
            a_image:req.file.path
        })
        .then(res.send('succeed'))
        .catch(error=>{
            res.status(400).send('failure')
        })
})

app.post('/attorney/delete',(req,res)=>{
    db('attorney')
        .where('id','=',req.body.id)
        .delete('*')
        .then(data=>{
            res.end()
        })
        .catch(res.status(400).send('error in deleted'))
})

app.post('/blogpost/add',upload.single('postImage'),(req,res)=>{
    const {p_title,p_writer,p_summary,p_details,p_date}=req.body;
    db('blogpost')
        .returning('*')
        .insert({
            p_title:p_title,
            p_writer:p_writer,
            p_summary:p_summary,
            p_details:p_details,
            p_image:req.file.path,
            p_date:p_date
        })
        .then(res.send('success'))
        .catch(error=>{
            res.status(400).send('error')
        })
})

app.get('/blogpost/all',(req,res)=>{
    db('blogpost')
        .select('id','p_title','p_writer','p_image','p_date','p_summary')
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('no post')
        })
})

app.post('/blogpost/view',(req,res)=>{
    db('blogpost')
        .where('id','=',req.body.id)
        .select('*')
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('not found')
        })
})

app.post('/blogpost/update',(req,res)=>{
    const {id,p_title,p_writer,p_summary,p_details}=req.body;
    db('blogpost')
        .where('id','=',req.body.id)
        .update({
            p_title:p_title,
            p_writer:p_writer,
            p_summary:p_summary,
            p_details:p_details
        })
        .then(data=>{
            res.send('updated')
        })
        .catch(error=>{
            res.status(400).send('not update')
        })
})

app.post('/blogpost/update/image',upload.single('postImage'),(req,res)=>{
    db('blogpost')
        .where('id','=',req.body.id)
        .update({
            p_image:req.file.path
        })
        .then(res.send('succeed'))
        .catch(error=>{
            res.status(400).send('failure')
        })
})

app.post('/blogpost/delete',(req,res)=>{
    db('blogpost')
        .where('id','=',req.body.id)
        .delete('*')
        .then(data=>{
            res.end()
        })
        .catch(res.status(400).send('error in deleted'))
})

app.post('/contactus',(req,res)=>{
    const {fname,lname,mobile,email,message}=req.body;
    db('contactus')
        .returning('*')
        .insert({
            fname:fname,
            lname:lname,
            mobile:mobile,
            email:email,
            message:message
        })
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('error in insert')
        })
})

app.get('/contactus/view',(req,res)=>{
    db('contactus')
        .select('*')
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('no records')
        })
})

app.post('/contactus/update',(req,res)=>{
    db('contactus')
        .where('id','=',req.body.id)
        .update({
            status:req.body.status
        })
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('not updated')
        })
})

app.get('/contactus/view/status',(req,res)=>{
    db('contactus')
        .where('status',null)
        .select('*')
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('not found')
        })
})

app.get('/lawfirm/info',(req,res)=>{
    db('lawfirminfo')
        .select('*')
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('no record found')
        })
})
app.get('/contactinfo/info',(req,res)=>{
    db('contactinfo')
        .select('*')
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('no record found')
        })
})
app.get('/openhours/info',(req,res)=>{
    db('openhours')
        .select('*')
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('no record found')
        })
})
app.get('/socialmedia/info', (req, res)=> {
    db('socialmedia')
        .select('*')
        .then(data=> {
            res.json(data)
        })
        .catch(error=> {
            res.status(400).json('no record found')
        })
})
app.post('/lawfirm/info/update',(req,res)=>{
    db('lawfirminfo')
        .where('id','=',req.body.id)
        .update({
            lawfirm:req.body.lawfirm
        })
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('error in update')
        })
})
app.post('/contactinfo/info/update',(req,res)=>{
    const {address,mobile,email,website,id}=req.body;
    db('contactinfo')
        .where('id','=',id)
        .update({
            address:address,
            mobile:mobile,
            email:email,
            website:website
        })
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('error in update')
        })
})
app.post('/openhours/info/update',(req,res)=>{
    const {id,montothru,fri,sat}=req.body;
    db('openhours')
        .where('id','=',id)
        .update({
            montothru:montothru,
            fri:fri,
            sat:sat
        })
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('error in update')
        })
})
app.post('/socialmedia/info/update',(req,res)=>{
    const {id,fb,tw,insta,lin}=req.body;
    db('socialmedia')
        .where('id','=',id)
        .update({
            fb:fb,
            tw:tw,
            insta:insta,
            lin:lin
        })
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('error in update')
        })
})

app.post('/adminlogin/register',(req,res)=>{
    const {fname,lname,email,uname,password,passkey}=req.body;
    const hash1=bcrypt.hashSync(password);
    const hash2=bcrypt.hashSync(passkey);
    db('adminlogin')
        .returning('*')
        .insert({
            fname:fname,
            lname:lname,
            email:email,
            uname:uname,
            password:hash1,
            passkey:hash2
        })
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json(error+"error found")
        })
})
app.post('/adminlogin/login',(req,res)=>{
    const {uname,password}=req.body;
    db('adminlogin')
        .select('email','password')
        .where('uname','=',uname)
        .then(data=>{
            const isValid=bcrypt.compareSync(password,data[0].password)
            if(isValid){
                db('adminlogin')
                    .select('fname','lname','uname')
                    .where('uname','=',uname)
                    .then(data=>{
                        res.json(data[0])
                    })
                    .catch(error=>{
                        res.status(400).json("failure")
                    })
            }else{
                res.status(400).json('password not matched')
            }
        })
        .catch(err => res.status(400).json('Invalid credential'))
})

app.post('/adminlogin/passkey',(req,res)=>{
    let passkey;
    db('adminlogin')
        .select('passkey')
        .then(data=>{
            passkey = bcrypt.compareSync(req.body.passkey,data[0].passkey);
            const passkey1 = bcrypt.compareSync(req.body.passkey,data[1].passkey)
            const passkey2 = bcrypt.compareSync(req.body.passkey,data[2].passkey)
            if (passkey || passkey1 || passkey2){
                res.json('success')
            }else {
                res.status(400).json('passkey not matched')
            }
        })
        .catch(error=>{
            res.status(400).json(error)
        })
})
app.post('/adminlogin/getuser',(req,res)=>{
    db('adminlogin')
        .select('id','fname','uname','lname','email')
        .where('uname','=',req.body.uname)
        .then(data=>{
            res.json(data)
        })
        .catch(error=>{
            res.status(400).json('user not found')
        })
})

app.post('/adminlogin/update/password',(req,res)=>{
    const {id,fname,lname,uname,password,newpassword,email}=req.body;
    db('adminlogin')
        .select('password')
        .where('id','=',id)
        .then(data=>{
            const oldpass=bcrypt.compareSync(password,data[0].password);
            if (oldpass){
                const newpass = bcrypt.hashSync(newpassword);
                db('adminlogin')
                    .whereNot('email',email)
                    .then(
                        db('adminlogin')
                            .where('id','=',id)
                            .update({
                                fname:fname,
                                lname:lname,
                                uname:uname,
                                password:newpass,

                            })
                            .then(data=>{
                                res.json('user data updated')
                            })
                            .catch(error=>{
                                res.status(400).json('existed')
                            })
                    )
                    .catch(error=>{
                        res.status(400).json('not updated')
                    })
            }else{
                res.json('old password not matched')
            }
        })
        .catch(error=>{
            res.status(400).json('not updated')
        })
})

app.post('/adminlogin/update/passkey',(req,res)=>{
    const {id,passkey,newpasskey}=req.body;
    db('adminlogin')
        .select('passkey')
        .where('id','=',id)
        .then(data=>{
            const oldpkey=bcrypt.compareSync(passkey,data[0].passkey);
            if (oldpkey){
                const newpkey = bcrypt.hashSync(newpasskey);
                db('adminlogin')
                    .where('id','=',id)
                    .update({
                        passkey:newpkey
                    })
                    .then(data=>{
                        res.json('passkey updated')
                    })
                    .catch(error=>{
                        res.status(400).json('not updated')
                    })
            }else{
                res.json('old passkey not matched')
            }
        })
        .catch(error=>{
            res.status(400).json('not updated')
        })
})

app.listen(process.env.PORT || 5000,()=>{
    console.log("server running on port");
});
